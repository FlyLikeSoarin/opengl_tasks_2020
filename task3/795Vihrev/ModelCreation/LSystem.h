#ifndef LSYSTEM_H
#define LSYSTEM_H

#include <string>
#include <vector>
#include <cstdarg>

using std::string;
using std::vector;


class LSystem {
public:
    LSystem(string alphabet, std::initializer_list<std::pair<char, string>> l);
    string produce(string axiom, int iterations);
private:
    string alphabet;
    vector<string> rules;
};


#endif
