#include "Trees.h"


void GrassTree::resolve_program_step(char step) {
    switch (step) {
        case 'F': {
            create_branch();
            state().position += state().front * state().length;
            return;
        }
        default: {
            resolve_program_step_common(step, 20.0f);
            return;
        }
    }
}


void SympodialTree::resolve_program_step(char step) {
    switch (step) {
        case 'F': {
            create_branch(0.707f);
            state().position += state().front * state().length;
            state().bottom_radius = state().bottom_radius * 0.707f;
            return;
        }
        case 'B': {
            state().left = glm::normalize(glm::vec4(glm::cross(glm::vec3(up), glm::vec3(state().front)), 1.0f));
            create_leaf(3.0f);
            create_leaf(2.0f);
            return;
        }
        case '?': {
            state().length = state().length * 0.9;
            return;
        }
        case '!': {
            state().length = state().length * 0.8;
            return;
        }
        default: {
            resolve_program_step_common(step, -33.0f, 48.0f, 35.0f, 45.0f, 180.0f,10.0f);
            return;
        }
    }\
}