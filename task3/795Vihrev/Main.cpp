#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <chrono>
#include <experimental/filesystem>
#include <iostream>
#include <vector>
#include <string>

#include "Application.h"

#include "GLPrimitives/Texture.h"
#include "GLPrimitives/Renderer.h"
#include "GLPrimitives/Camera.h"
#include "GLPrimitives/IndexBufferObject.h"
#include "GLPrimitives/ModelMatrix.h"
#include "GLPrimitives/ShaderProgram.h"
#include "GLPrimitives/VertexArrayObject.h"
#include "GLPrimitives/VertexBufferObject.h"

#include "ModelCreation/Trees.h"
#include "Utils.h"

const GLfloat move_speed = 0.01f;


class TreeApplication : public Application {
public:
    void setup() override {
        Application::setup();

        auto default_program = new ShaderProgram(
                read_file("795VihrevData3/default.vertex.shader"),
                read_file("795VihrevData3/default.fragment.shader"));

        auto instanced_program = new ShaderProgram(
                read_file("795VihrevData3/instanced.vertex.shader"),
                read_file("795VihrevData3/default.fragment.shader"));

        program_index.push_back(default_program);
        program_index.push_back(instanced_program);

        auto tree = new SympodialTree();
        tree->generate(
               LSystem("", {
                   {'A', "F[&?B]/[^!B"},
                   {'B', "F[+?$B]\\[-!$B]"}
               }),
               "A", 10, 0.8f, 4.0f
        );

        // Branch vertex and index buffers
        auto v_data_p = tree->get_v_data_branch();
        auto i_data_p = tree->get_i_data_branch();

        auto vbo = new VertexBufferObject(reinterpret_cast<void *>(v_data_p.first), v_data_p.second, sizeof(GLfloat));
        auto ibo = new IndexBufferObject(i_data_p.first, i_data_p.second);

        auto vao = new VertexArrayObject();
        vao->add_attribute(Attribute(3));
        vao->add_attribute(Attribute(3));
        vao->add_attribute(Attribute(3));
        vao->add_attribute(Attribute(2));
        vao->init();

        auto model_matrix = new ModelMatrix();

        vao_index.push_back(vao);
        vbo_index.push_back(vbo);
        ibo_index.push_back(ibo);
        model_matrix_index.push_back(model_matrix);

        render_targets.emplace_back(instanced_program, vao, vbo, ibo, model_matrix);

        // Leaf vertex and index buffers
        v_data_p = tree->get_v_data_leaf();
        i_data_p = tree->get_i_data_leaf();

        vbo = new VertexBufferObject(reinterpret_cast<void *>(v_data_p.first), v_data_p.second, sizeof(GLfloat));
        ibo = new IndexBufferObject(i_data_p.first, i_data_p.second);

        vao = new VertexArrayObject();
        vao->add_attribute(Attribute(3));
        vao->add_attribute(Attribute(3));
        vao->add_attribute(Attribute(3));
        vao->add_attribute(Attribute(2));
        vao->init();

        vbo_index.push_back(vbo);
        ibo_index.push_back(ibo);

        render_targets.emplace_back(instanced_program, vao, vbo, ibo, model_matrix);

        GLfloat points[] = {
                // x      y     z    nx    ny    nz    ux    uy    uz    u     v
                75.0f, 75.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 5.0f, 5.0f,
                75.0f, -75.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 5.0f, -5.0f,
                -75.0f, 75.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, -5.0f, 5.0f,
                -75.0f, -50.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, -5.0f, -5.0f
        };

        GLuint indices[] = {
                0, 1, 2, 1, 3, 2
        };

        vbo = new VertexBufferObject(reinterpret_cast<void *>(points), 4 * 11, sizeof(GLfloat));
        ibo = new IndexBufferObject(indices, 6);

        vao = new VertexArrayObject();
        vao->add_attribute(Attribute(3));
        vao->add_attribute(Attribute(3));
        vao->add_attribute(Attribute(3));
        vao->add_attribute(Attribute(2));
        vao->init();

        render_targets.emplace_back(default_program, vao, vbo, ibo, model_matrix);

        delete tree;

        texture_index.push_back(new Texture("./795VihrevData3/tree-bark-texture.tga"));
        texture_index.push_back(new Texture("./795VihrevData3/tree-bark-normal.tga"));

        texture_index.push_back(new Texture("./795VihrevData3/tree-leaf-texture.tga"));
        texture_index.push_back(new Texture("./795VihrevData3/tree-leaf-normal.tga"));
        texture_index.push_back(new Texture("./795VihrevData3/tree-leaf-alpha.tga"));

        texture_index.push_back(new Texture("./795VihrevData3/grass-texture.jpg"));

        light_direction = new Camera(glm::normalize(glm::vec3(1.0f)), glm::vec3(0.0f));
    }

    void before_draw() override {
        Application::before_draw();
        camera->move(move_direction * glm::length(camera->get_direction()));
    }

    void draw() override {
        int j = 0;
        for (auto& render_target : render_targets) {
            if (j==0) {
                for (int i = 0; i < 2; ++i) {
                    texture_index[i]->bind(GL_TEXTURE0 + i);
                }
            } else if (j==1) {
                for (int i = 0; i < 3; ++i) {
                    texture_index[i + 2]->bind(GL_TEXTURE0 + i);
                }
            } else {
                texture_index[5]->bind(GL_TEXTURE0);
            }

            auto program = render_target.program;

            program->set_uniform_f3("u_DirectionalLight", light_direction->get_direction());
            program->set_uniform_i1("u_TextureSampler", 0);

            program->set_uniform_i1("u_UseNormal", j==2 ? 0 : 1);
            program->set_uniform_i1("u_NormalSampler", 1);

            program->set_uniform_i1("u_UseAlpha", j==1 ? 1 : 0);
            program->set_uniform_i1("u_AlphaSampler", 2);
            if (j == 2) {
                renderer->render(render_target.program, render_target.vao, render_target.vbo, render_target.ibo,
                                 render_target.model_matrix->get());
            } else {
                program->use();
                render_target.vao->bind();
                render_target.vbo->bind();
                render_target.ibo->bind();

                glm::mat4 matrices[] = {
                        glm::translate(glm::mat4(1.0), glm::vec3(0.0f, 0.0f, 0.0f)),
                        glm::translate(glm::mat4(1.0), glm::vec3(0.0f, 35.0f, 0.0f)),
                        glm::translate(glm::mat4(1.0), glm::vec3(45.0f, 10.0f, 0.0f)),
                        glm::translate(glm::mat4(1.0), glm::vec3(35.0f, -35.0f, 0.0f)),
                        glm::translate(glm::mat4(1.0), glm::vec3(-35.0f, 17.0f, 0.0f)),
                        glm::translate(glm::mat4(1.0), glm::vec3(-45.0f, 62.0f, 0.0f)),
                        glm::translate(glm::mat4(1.0), glm::vec3(45.0f, -62.0f, 0.0f)),
                        glm::translate(glm::mat4(1.0), glm::vec3(-45.0f, -10.0f, 0.0f)),
                };

                program->set_uniform_mat4f("u_MVPMatrix", camera->view_projection_matrix());
                program->set_uniform_mat4fv("u_InstanceMatrices", matrices, 8);

                program->bind_uniforms();

                glDrawElementsInstanced(GL_TRIANGLES, int(render_target.ibo->get_count()), GL_UNSIGNED_INT, nullptr, 8);
            }
            j++;
        }
    }

    void after_draw() override {
        Application::after_draw();
        program_index[0]->clear_uniform();
    }

    void teardown() override {
        Application::teardown();
    }

    void on_mouse_button_event(int button, int action, int mods) override {
        Application::on_mouse_button_event(button, action, mods);
        if (button == GLFW_MOUSE_BUTTON_RIGHT) {
            if (action == GLFW_PRESS) {
                rb_down = true;
            } else if(action == GLFW_RELEASE) {
                rb_down = false;
            }
        }
        if (button == GLFW_MOUSE_BUTTON_LEFT) {
            if (action == GLFW_PRESS) {
                lb_down = true;
            } else if(action == GLFW_RELEASE) {
                lb_down = false;
            }
        }
    }

    void on_mouse_wheel_event(double x_offset, double y_offset) override {
        Application::on_mouse_wheel_event(x_offset, y_offset);
        camera->move_in_orbit(0.0f, 0.0f, GLfloat(y_offset));
    }

    void on_mouse_position_event(double _x_pos, double _y_pos) override {
        Application::on_mouse_position_event(x_pos, y_pos);
        if (rb_down) {
            camera->move_in_orbit(GLfloat(_x_pos) - x_pos, GLfloat(_y_pos) - y_pos);
        }
        if (lb_down) {
            light_direction->move_in_orbit(GLfloat(_x_pos) - x_pos, GLfloat(_y_pos) - y_pos);
        }
        x_pos = GLfloat(_x_pos);
        y_pos = GLfloat(_y_pos);
    }

    void on_keyboard_event(int key, int scancode, int action, int mods) override {
        Application::on_keyboard_event(key, scancode, action, mods);
        if (key == GLFW_KEY_E) {
            if (action == GLFW_PRESS) {
                move_direction = glm::vec3(move_direction.x, move_direction.y, move_speed);
            } else if(action == GLFW_RELEASE) {
                move_direction = glm::vec3(move_direction.x, move_direction.y, 0.0f);
            }
        }
        if (key == GLFW_KEY_Q) {
            if (action == GLFW_PRESS) {
                move_direction = glm::vec3(move_direction.x, move_direction.y, -move_speed);
            } else if(action == GLFW_RELEASE) {
                move_direction = glm::vec3(move_direction.x, move_direction.y, 0.0f);
            }
        }
    }

    bool lb_down = false;
    Camera* light_direction;

    bool rb_down = false;
    glm::vec3 move_direction = glm::vec3(0.0f);
    GLfloat x_pos = 0.0f;
    GLfloat y_pos = 0.0f;
    GLfloat scroll = 0.0f;
};

int main() {
    auto app = TreeApplication();
    app.start();
}
