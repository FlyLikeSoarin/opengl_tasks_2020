set(SRC_FILES
        Main.cpp
        Application.cpp
        GLPrimitives/Camera.cpp
        GLPrimitives/IndexBufferObject.cpp
        GLPrimitives/ModelMatrix.cpp
        GLPrimitives/Renderer.cpp
        GLPrimitives/ShaderProgram.cpp
        GLPrimitives/Texture.cpp
        GLPrimitives/VertexArrayObject.cpp
        GLPrimitives/VertexBufferObject.cpp
        ModelCreation/Figures.cpp
        ModelCreation/LSystem.cpp
        ModelCreation/BaseTree.cpp
        ModelCreation/Trees.cpp)

MAKE_OPENGL_TASK(795Vihrev 3 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(795Vihrev3 stdc++fs)
endif()