#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <fstream>
#include <string>


std::string read_file(std::string filename) {
    std::ifstream ifs(filename);
    if (!ifs.is_open()) {
        std::cerr << "Could not open the file - '"
                  << filename << "'" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string data( (std::istreambuf_iterator<char>(ifs) ),
                      (std::istreambuf_iterator<char>()    ) );
    return data;
}

#endif
