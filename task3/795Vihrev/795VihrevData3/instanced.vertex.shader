#version 330

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec3 vertexUp;
layout(location = 3) in vec2 vertexTextureCoords;

uniform mat4 u_MVPMatrix;
uniform vec3 u_DirectionalLight;

uniform mat4 u_InstanceMatrices[9];

out vec2 TextureCoords;
out vec3 Light;

void main()
{
   gl_Position = u_MVPMatrix * u_InstanceMatrices[gl_InstanceID] * vec4(vertexPosition, 1.0);
   TextureCoords = vertexTextureCoords;

   vec3 left = cross(-vertexNormal, vertexUp);
   Light = vec3(
      dot(left, u_DirectionalLight),
      dot(vertexUp, u_DirectionalLight),
      dot(-vertexNormal, u_DirectionalLight)
   );
}