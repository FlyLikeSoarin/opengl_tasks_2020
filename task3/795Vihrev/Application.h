#ifndef APPLICATION_H
#define APPLICATION_H

#include <iostream>
#include <set>
#include <vector>
#include <memory>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "GLPrimitives/Texture.h"
#include "GLPrimitives/Renderer.h"
#include "GLPrimitives/Camera.h"
#include "GLPrimitives/IndexBufferObject.h"
#include "GLPrimitives/ModelMatrix.h"
#include "GLPrimitives/ShaderProgram.h"
#include "GLPrimitives/VertexArrayObject.h"
#include "GLPrimitives/VertexBufferObject.h"

using std::weak_ptr;
using std::shared_ptr;
using std::set;
using std::vector;


struct RenderTarget {
    RenderTarget(
            ShaderProgram *program,
            VertexArrayObject *vao,
            VertexBufferObject *vbo,
            IndexBufferObject *ibo,
            ModelMatrix *model_matrix
    );

    ~RenderTarget();

    bool operator<(const RenderTarget &rhv) const;

    ShaderProgram *program;
    VertexArrayObject *vao;
    VertexBufferObject *vbo;
    ModelMatrix *model_matrix;
    IndexBufferObject *ibo;
};


class Application {
public:
    Application();
    virtual ~Application();

    void start();

    virtual void on_mouse_button_event(int button, int action, int mods);
    virtual void on_mouse_wheel_event(double x_offset, double y_offset);
    virtual void on_mouse_position_event(double x_pos, double y_pos);
    virtual void on_keyboard_event(int key, int scancode, int action, int mods);
    virtual void on_window_resize_event(int width, int height);

    virtual void configure();

    virtual void setup();

    virtual void before_draw();
    virtual void draw();
    virtual void after_draw();

    virtual void teardown();

    Camera* camera;
    Renderer* renderer;
    GLFWwindow* window;

    vector<RenderTarget> render_targets;

    vector<ShaderProgram*> program_index;
    vector<VertexArrayObject*> vao_index;
    vector<VertexBufferObject*> vbo_index;
    vector<IndexBufferObject*> ibo_index;
    vector<ModelMatrix*> model_matrix_index;
    vector<Texture*> texture_index;
};


#endif
