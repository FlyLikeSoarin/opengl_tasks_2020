#include "VertexBufferObject.h"


VertexBufferObject::VertexBufferObject(void *data, long count, long size) {
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    //Копируем содержимое массива в буфер на видеокарте
    glBufferData(GL_ARRAY_BUFFER, count * size, data, GL_STATIC_DRAW);
}

VertexBufferObject::~VertexBufferObject() {
    glDeleteBuffers(1, &vbo);
}

GLuint VertexBufferObject::get() const {
    return vbo;
}

void VertexBufferObject::bind() {
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
}