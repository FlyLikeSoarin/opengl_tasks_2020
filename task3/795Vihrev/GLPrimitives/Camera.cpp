#include "Camera.h"


Camera::Camera(glm::vec3 _eye, glm::vec3 _center, glm::vec3 _up)
: eye(_eye), center(_center), up(_up), view_projection_matrix_cache(glm::mat4(1.0f)) {
    update_left();
}

Camera::~Camera() {}

glm::mat4 Camera::update_view_projection_matrix() {
    auto projection_matrix = glm::perspective(
            glm::radians(45.0f), // The vertical Field of View, in radians: the amount of "zoom". Think "camera lens". Usually between 90° (extra wide) and 30° (quite zoomed in)
            aspect_ratio,       // Aspect Ratio. Depends on the size of your window. Notice that 4/3 == 800/600 == 1280/960, sounds familiar ?
            0.1f,              // Near clipping plane. Keep as big as possible, or you'll get precision issues.
            150.0f);
    auto view_matrix = glm::lookAt(eye, center, up);

    view_projection_matrix_cache = projection_matrix * view_matrix;
    matrix_ready = true;
    return view_projection_matrix_cache;
}

glm::mat4 Camera::view_projection_matrix() {
    return matrix_ready ? view_projection_matrix_cache : update_view_projection_matrix();
}

void Camera::set_screen_size(int width, int height) {
    aspect_ratio = GLfloat(width) / GLfloat(height);
    matrix_ready = false;
}

void Camera::set_fov(GLfloat _fov) {
    fov = _fov;
    matrix_ready = false;
}

void Camera::look_in_direction(glm::vec3 direction, GLfloat distance, bool _update_left) {
    if (distance == 0.0f) {
        distance = glm::length(center - eye);
    }
    eye = center - glm::normalize(direction) * distance;
    if (_update_left) {
        update_left();
    }
    matrix_ready = false;
}

glm::vec3 Camera::get_direction() const {
    return center - eye;
}

void Camera::update_left() {
    auto dot = glm::dot(up, glm::normalize(get_direction()));
    if (glm::abs(glm::abs(dot) - 1.0f) < 0.00001f) {
        left = glm::vec3(0.0f, 1.0f, 0.0f);
    } else {
        left = glm::normalize(glm::cross(up, get_direction()));
    }
}

void Camera::move_in_orbit(GLfloat horizontal_delta, GLfloat vertical_delta, GLfloat distance_delta) {
    auto direction = get_direction();
    auto distance = glm::length(direction) + distance_delta;
    distance = distance >= 0.0f ? distance : 0.0f;
    direction = glm::normalize(direction);

    auto h_rotate = glm::rotate(glm::mat4(1.0f), glm::radians(horizontal_delta), up);
    direction = h_rotate * glm::vec4(direction, 0.0f);
    left = h_rotate * glm::vec4(left, 0.0f);

    auto v_rotate = glm::rotate(glm::mat4(1.0f), glm::radians(vertical_delta), left);
    direction = v_rotate * glm::vec4(direction, 0.0f);

    look_in_direction(direction, distance, false);
}

void Camera::move(glm::vec3 direction) {
    center += direction;
    eye += direction;
    matrix_ready = false;
}

glm::vec3 Camera::getEye() const {
    return eye;
}

glm::vec3 Camera::getCenter() const {
    return center;
}
