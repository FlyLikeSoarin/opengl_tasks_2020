#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/ext/matrix_clip_space.hpp>


class Camera {
public:
    Camera(glm::vec3 eye, glm::vec3 center, glm::vec3 up = glm::vec3(0.0f, 0.0f, 1.0f));
    ~Camera();

    glm::mat4 view_projection_matrix();
    glm::vec3 get_direction() const;
    glm::vec3 getEye() const;
    glm::vec3 getCenter() const;

    void look_in_direction(glm::vec3 direction, GLfloat distance=0.0f, bool update_left=true);
    void move_in_orbit(GLfloat horizontal_delta=0.0f, GLfloat vertical_delta=0.0f, GLfloat distance_delta=0.0f);
    void move(glm::vec3 direction);

    void set_screen_size(int width, int height);
    void set_fov(GLfloat fov);
private:
    void update_left();
    glm::mat4 update_view_projection_matrix();

    glm::vec3 eye;
    glm::vec3 center;
    glm::vec3 up;
    glm::vec3 left;

    GLfloat fov = 45.0f;
    GLfloat aspect_ratio = 3.0f/4.0f;
    GLfloat near_plane = 0.1f;
    GLfloat far_plane = 100.f;

    bool matrix_ready = false;
    glm::mat4 view_projection_matrix_cache;
};


#endif
