#include "VertexArrayObject.h"


VertexArrayObject::VertexArrayObject() {}

VertexArrayObject::~VertexArrayObject() {
    glDeleteVertexArrays(1, &vao);
}

void VertexArrayObject::add_attribute(Attribute attribute) {
    attributes.push_back(attribute);
    stride += attribute.size * attribute.count;
}

void VertexArrayObject::init() {
    glGenVertexArrays(1, &vao);

    //Делаем этот объект текущим
    glBindVertexArray(vao);

    int current_index = 0;
    size_t offset = 0;

    for (auto attribute : attributes) {
        //Включаем вершинный атрибут под номером current_index
        glEnableVertexAttribArray(current_index);

        glVertexAttribPointer(
                current_index, // attribute index
                attribute.count, // number of generic elements in attribute
                attribute.type,
                attribute.normalized, // will normalize values if true
                stride, // offset between two of the same attributes
                reinterpret_cast<void *>(offset) // offset before first instance of this attribute
        );
        offset += attribute.size * attribute.count;
        ++current_index;
    }

    attributes.clear();
    glBindVertexArray(0);
}

void VertexArrayObject::bind() {
    glBindVertexArray(vao);
}

bool VertexArrayObject::is_initialized() const {
    return vao != 0;
}

GLuint VertexArrayObject::get() const {
    return vao;
}
