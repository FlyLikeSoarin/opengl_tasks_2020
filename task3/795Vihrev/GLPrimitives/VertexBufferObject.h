#ifndef VERTEXBUFFEROBJECT_H
#define VERTEXBUFFEROBJECT_H

#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>


class VertexBufferObject {
public:
    VertexBufferObject(void* data, long count, long size);
    ~VertexBufferObject();

    GLuint get() const;
    void bind();
private:
    GLuint vbo = 0;
};


#endif
