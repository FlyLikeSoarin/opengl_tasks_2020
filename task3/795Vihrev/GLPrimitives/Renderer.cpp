#include "Renderer.h"


Renderer::Renderer() {}

Renderer::~Renderer() {}

void Renderer::use_camera(Camera *_camera) {
    camera = _camera;
}

void Renderer::render(ShaderProgram* program, VertexArrayObject* vao, VertexBufferObject* vbo, IndexBufferObject* ibo, glm::mat4 model_matrix) {
//    if (program->get() != previous_program) program->use();
//    if (vao->get() != previous_vao) vao->bind();
//    if (vbo->get() != previous_vbo) vbo->bind();
//    if (ibo->get() != previous_ibo) ibo->bind();
    program->use();
    vao->bind();
    vbo->bind();
    ibo->bind();

    program->set_uniform_mat4f("u_MVPMatrix", camera->view_projection_matrix() * model_matrix);

    program->bind_uniforms();

    glDrawElements(GL_TRIANGLES, int(ibo->get_count()), GL_UNSIGNED_INT, nullptr);
}