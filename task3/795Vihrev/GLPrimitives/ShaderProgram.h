#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include <string>
#include <unordered_map>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>


union glm_any {
    glm::vec3 f3;
    GLfloat f1;
    GLint i1;
    glm::mat4 mat4f;
    glm::mat4* mat4fv;
};


class ShaderProgram {
public:
    ShaderProgram(std::string vertex_shader, std::string fragment_shader);
    ~ShaderProgram();

    GLuint get() const;
    void use();

    void set_uniform_f3(const std::string& name, glm::vec3 f3);
    void set_uniform_f1(const std::string& name, GLfloat f1);
    void set_uniform_i1(const std::string& name, GLint i1);
    void set_uniform_mat4f(const std::string& name, glm::mat4 mat4f);
    void set_uniform_mat4fv(const std::string& name, glm::mat4* mat4f, int count);

    void bind_uniforms();
    void clear_uniform();

private:
    int count;
    GLuint program;
    std::unordered_map<std::string, std::pair<std::string, glm_any>> uniforms;
};


#endif
