#include "ShaderProgram.h"

#include <iostream>
#include <vector>


ShaderProgram::ShaderProgram(std::string vertex_shader_source, std::string fragment_shader_source) {
    //Вершинный шейдер
    const char* vertexShaderText = vertex_shader_source.c_str();

    //Создаем шейдерный объект
    GLuint vs = glCreateShader(GL_VERTEX_SHADER);

    //Передаем в шейдерный объект текст шейдера
    glShaderSource(vs, 1, &vertexShaderText, nullptr);

    //Компилируем шейдер
    glCompileShader(vs);

    //Проверяем ошибки компиляции
    int status = -1;
    glGetShaderiv(vs, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint errorLength;
        glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetShaderInfoLog(vs, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to compile the shader:\n" << errorMessage.data() << std::endl;

        exit(1);
    }

    //=========================================================

    //Фрагментный шейдер
    const char* fragmentShaderText = fragment_shader_source.c_str();

    //Создаем шейдерный объект
    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);

    //Передаем в шейдерный объект текст шейдера
    glShaderSource(fs, 1, &fragmentShaderText, nullptr);

    //Компилируем шейдер
    glCompileShader(fs);

    //Проверяем ошибки компиляции
    status = -1;
    glGetShaderiv(fs, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint errorLength;
        glGetShaderiv(fs, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetShaderInfoLog(fs, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to compile the shader:\n" << errorMessage.data() << std::endl;

        exit(1);
    }

    //=========================================================

    //Создаем шейдерную программу
    program = glCreateProgram();

    //Прикрепляем шейдерные объекты
    glAttachShader(program, vs);
    glAttachShader(program, fs);

    //Линкуем программу
    glLinkProgram(program);

    //Проверяем ошибки линковки
    status = -1;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint errorLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetProgramInfoLog(program, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to link the program:\n" << errorMessage.data() << std::endl;

        exit(1);
    }

    glDetachShader(program, vs);
    glDetachShader(program, fs);

    glDeleteShader(vs);
    glDeleteShader(fs);
}

ShaderProgram::~ShaderProgram() {
    glDeleteProgram(program);
}

GLuint ShaderProgram::get() const {
    return program;
}

void ShaderProgram::use() {
    glUseProgram(program);
}

void ShaderProgram::bind_uniforms() {
    for (auto& elem : uniforms) {
        auto name = elem.first.c_str();
        auto type = elem.second.first;
        auto data = elem.second.second;

        GLint location = glGetUniformLocation(program, name);

        if (type == "f3") {
            glUniform3f(location, data.f3.x, data.f3.y, data.f3.z);
        }
        else if (type == "f1") {
            glUniform1f(location, data.f1);
        }
        else if (type == "i1") {
            glUniform1i(location, data.i1);
        }
        else if (type == "mat4f") {
            glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(data.mat4f));
        }
        else if (type == "mat4fv") {
            GLfloat* value = new GLfloat[4 * 16 * count];
            for (int i = 0; i < count; ++i) {
                for (int j = 0; j < 16; ++j) {
                    value[i*16 + j] = glm::value_ptr(data.mat4fv[i])[j];
                }
            }

            glUniformMatrix4fv(location, count, GL_FALSE, value);
            delete[] value;
        }
        else {}
    }
}

void ShaderProgram::set_uniform_f3(const std::string& name, glm::vec3 f3) {
    glm_any data;
    data.f3 = f3;
    uniforms[name] = std::make_pair("f3", data);
}

void ShaderProgram::set_uniform_f1(const std::string& name, GLfloat f1) {
    glm_any data;
    data.f1 = f1;
    uniforms[name] = std::make_pair("f1", data);
}

void ShaderProgram::set_uniform_i1(const std::string& name, GLint i1) {
    glm_any data;
    data.i1 = i1;
    uniforms[name] = std::make_pair("i1", data);
}

void ShaderProgram::set_uniform_mat4f(const std::string &name, glm::mat4 mat4f) {
    glm_any data;
    data.mat4f = mat4f;
    uniforms[name] = std::make_pair("mat4f", data);
}

void ShaderProgram::set_uniform_mat4fv(const std::string &name, glm::mat4 *mat4fv, int _count) {
    glm_any data;
    data.mat4fv = mat4fv;
    count = _count;
    uniforms[name] = std::make_pair("mat4fv", data);
}

void ShaderProgram::clear_uniform() {
    uniforms.clear();
}
