#version 330

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec3 vertexUp;
layout(location = 3) in vec2 vertexTextureCoords;

uniform mat4 u_MVPMatrix;
uniform vec3 u_DirectionalLight;

out vec3 Normal;

void main()
{
   gl_Position = u_MVPMatrix * vec4(vertexPosition, 1.0);
   Normal = vertexNormal;
}