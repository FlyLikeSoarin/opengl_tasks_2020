#version 330

in vec3 Normal;
out vec4 color;

void main()
{
    color = color = vec4(Normal, 1.0) * 0.5 + 0.5;
}