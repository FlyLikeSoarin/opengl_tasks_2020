#include "Application.h"

RenderTarget::RenderTarget(
        ShaderProgram* _program,
        VertexArrayObject* _vao,
        VertexBufferObject* _vbo,
        IndexBufferObject* _ibo,
        ModelMatrix* _model_matrix
): program(_program), vao(_vao), vbo(_vbo), ibo(_ibo), model_matrix(_model_matrix) {}

RenderTarget::~RenderTarget() {}

bool RenderTarget::operator<(const RenderTarget &rhv) const {
    return this->program->get() < rhv.program->get() || this->vao->get() < rhv.vao->get() ||
            this->vbo->get() < rhv.vbo->get() || this->ibo->get() < rhv.ibo->get();
}


void keyboard_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    auto app = (Application*) glfwGetWindowUserPointer(window);
    app->on_keyboard_event(key, scancode, action, mods);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
    auto app = (Application*) glfwGetWindowUserPointer(window);
    app->on_mouse_button_event(button, action, mods);
}

void mouse_wheel_callback(GLFWwindow* window, double x_offset, double y_offset) {
    auto app = (Application*) glfwGetWindowUserPointer(window);
    app->on_mouse_wheel_event(x_offset, y_offset);
}

void mouse_position_callback(GLFWwindow* window, double x_pos, double y_pos) {
    auto app = (Application*) glfwGetWindowUserPointer(window);
    app->on_mouse_position_event(x_pos, y_pos);
}


Application::Application() {

}

Application::~Application() {

}

void Application::start() {
    configure();

    setup();

    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();

        before_draw();

        int width, height;
        glfwGetFramebufferSize(window, &width, &height);

        glViewport(0, 0, width, height);

        on_window_resize_event(width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        draw();

        glfwSwapBuffers(window);

        after_draw();
    }

    teardown();

    glfwTerminate();
}

void Application::configure() {
    // GLFW
    if (!glfwInit())
    {
        std::cerr << "ERROR: could not start GLFW3\n";
        exit(1);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(1600, 1200, "MIPT OpenGL demos", nullptr, nullptr);
    if (!window)
    {
        std::cerr << "ERROR: could not open window with GLFW3\n";
        glfwTerminate();
        exit(1);
    }

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    glfwSetWindowUserPointer(window, this);

    glfwSetKeyCallback(window, keyboard_callback);
    glfwSetCursorPosCallback(window, mouse_position_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetScrollCallback(window, mouse_wheel_callback);

    // GLEW
    glewExperimental = GL_TRUE;
    glewInit();

    glFrontFace(GL_CW);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    for(;;) {
        auto err = glGetError();
        if (err == GL_NO_ERROR) break;
        printf("Error %d: %s\n", err, glewGetErrorString(err));
    }
}

void Application::setup() {
    camera = new Camera(glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f));
    renderer = new Renderer();
    renderer->use_camera(camera);
}

void Application::before_draw() {

}

void Application::draw() {
    for (auto& render_target : render_targets) {
        renderer->render(render_target.program, render_target.vao, render_target.vbo, render_target.ibo, render_target.model_matrix->get());
    }
}

void Application::after_draw() {

}

void Application::teardown() {
    for (auto i : program_index) { delete i; }
    for (auto i : vao_index) { delete i; }
    for (auto i : vbo_index) { delete i; }
    for (auto i : ibo_index) { delete i; }
    for (auto i : model_matrix_index) { delete i; }
    for (auto i : texture_index) { delete i; }

    delete renderer;
    delete camera;
}

void Application::on_window_resize_event(int width, int height) {
    camera->set_screen_size(width, height);
}

void Application::on_mouse_button_event(int button, int action, int mods) {

}

void Application::on_mouse_wheel_event(double x_offset, double y_offset) {

}

void Application::on_mouse_position_event(double x_pos, double y_pos) {

}

void Application::on_keyboard_event(int key, int scancode, int action, int mods) {

}
