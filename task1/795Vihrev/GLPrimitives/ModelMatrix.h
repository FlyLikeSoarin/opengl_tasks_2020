#ifndef MESH_H
#define MESH_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "IndexBufferObject.h"
#include "VertexArrayObject.h"
#include "VertexBufferObject.h"


class ModelMatrix {
public:
    ModelMatrix();
    ~ModelMatrix();

    void set_scale(glm::vec3 scale);
    void set_rotation(glm::vec3 axis, glm::float32 angle);
    void set_position(glm::vec3 position);

    glm::mat4 get() const;
    void apply_transform(glm::mat4 transform_matrix);
    void clear_transform();

private:
    glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f);
    glm::vec3 axis = glm::vec3(1.0f, 0.0f, 0.0);
    glm::float32 angle = 0.0f;
    glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);

    glm::mat4 transform_matrix;
};


#endif
