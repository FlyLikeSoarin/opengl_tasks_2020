#include "IndexBufferObject.h"

IndexBufferObject::IndexBufferObject(GLuint *indices, long _count) : count(_count) {
    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * static_cast<long>(sizeof(GLuint)), indices, GL_STATIC_DRAW);
}

IndexBufferObject::~IndexBufferObject() {
    glDeleteBuffers(1, &ibo);
}

GLuint IndexBufferObject::get() const {
    return ibo;
}

long IndexBufferObject::get_count() const {
    return count;
}

void IndexBufferObject::bind() {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
}