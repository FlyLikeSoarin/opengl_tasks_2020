#ifndef OPENGL_TASKS_2020_RENDERER_H
#define OPENGL_TASKS_2020_RENDERER_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "ModelMatrix.h"
#include "Camera.h"
#include "ShaderProgram.h"


class Renderer {
public:
    Renderer();
    ~Renderer();

    void use_camera(Camera* camera);

    void render(ShaderProgram* program, VertexArrayObject* vao, VertexBufferObject* vbo, IndexBufferObject* ibo, glm::mat4 model_matrix);

private:
    Camera* camera;

    GLuint previous_program = 0;
    GLuint previous_vao = 0;
    GLuint previous_vbo = 0;
    GLuint previous_ibo = 0;
};


#endif //OPENGL_TASKS_2020_RENDERER_H
