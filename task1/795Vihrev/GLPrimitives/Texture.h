#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <SOIL2.h>

using std::string;


class Texture {
public:
    Texture(string filename);
    ~Texture();

    void bind(GLenum slot=GL_TEXTURE0);

private:
    GLuint texture;
};


#endif
