#include "ModelMatrix.h"


ModelMatrix::ModelMatrix(): transform_matrix(1.0f) {}
ModelMatrix::~ModelMatrix() {}

void ModelMatrix::set_scale(glm::vec3 _scale) { scale = _scale; }
void ModelMatrix::set_rotation(glm::vec3 _axis, glm::float32 _angle) { axis = _axis, angle = _angle; }
void ModelMatrix::set_position(glm::vec3 _position) { position = _position; }

glm::mat4 ModelMatrix::get() const {
    return transform_matrix
    * glm::translate(glm::mat4(1.0), position)
    * glm::rotate(glm::mat4(1.0), angle, axis)
    * glm::scale(glm::mat4(1.0), scale);
}

void ModelMatrix::apply_transform(glm::mat4 _transform_matrix) {
    transform_matrix = _transform_matrix * transform_matrix;
}

void ModelMatrix::clear_transform() {
    transform_matrix = glm::mat4(1.0f);
}