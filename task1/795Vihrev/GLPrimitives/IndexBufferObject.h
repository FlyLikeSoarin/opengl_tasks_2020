#ifndef INDEXBUFFEROBJECT_H
#define INDEXBUFFEROBJECT_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>


class IndexBufferObject {
public:
    IndexBufferObject(GLuint* indices, long count);
    ~IndexBufferObject();

    GLuint get() const;
    long get_count() const;
    void bind();
private:
    GLuint ibo = 0;
    long count;
};


#endif
