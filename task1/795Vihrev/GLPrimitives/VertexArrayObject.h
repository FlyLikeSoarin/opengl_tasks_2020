#ifndef VERTEXARRAYOBJECT_H
#define VERTEXARRAYOBJECT_H

#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>


struct Attribute {
    Attribute(GLint _count, GLenum _type=GL_FLOAT, GLboolean _normalized=GL_FALSE, size_t _size=4)
    : count(_count), type(_type), normalized(_normalized), size(_size) {}
    GLint count;
    GLenum type;
    GLboolean normalized;
    size_t size;
};


class VertexArrayObject {
public:
    VertexArrayObject();
    ~VertexArrayObject();

    void add_attribute(Attribute attribute);

    void init();

    void bind();

    bool is_initialized() const;

    GLuint get() const;

private:
    GLuint vao = 0;
    size_t stride = 0;
    std::vector<Attribute> attributes;
};


#endif
