#include "Figures.h"

#include <iostream>

using namespace figures;
using std::make_pair;

// --- Helpers ---

static void allocate(sizes _sizes, GLfloat** v_data, GLuint** i_data, int stride) {
    (*v_data) = new GLfloat[_sizes.first * stride];
    (*i_data) = new GLuint[_sizes.second * 3];
}

// --- Transforms ---

void figures::apply_transform(glm::mat4 transform_matrix, int vertices, GLfloat *v_data, int stride) {
    for (int i = 0; i < vertices; ++i) {
        GLfloat *vertex = &v_data[i * stride];
        auto position = glm::vec4(vertex[0], vertex[1], vertex[2], 1.0f);
        position = transform_matrix * position;
        vertex[0] = position.x;
        vertex[1] = position.y;
        vertex[2] = position.z;

        position = glm::vec4(vertex[3], vertex[4], vertex[5], 0.0f);
        position = glm::normalize(transform_matrix * position);
        vertex[3] = position.x;
        vertex[4] = position.y;
        vertex[5] = position.z;

        position = glm::vec4(vertex[6], vertex[7], vertex[8], 0.0f);
        position = glm::normalize(transform_matrix * position);
        vertex[6] = position.x;
        vertex[7] = position.y;
        vertex[8] = position.z;
    }
}

// --- Figure creation ---

sizes figures::circle_sizes(int vertices_per_circle) {
    return make_pair(vertices_per_circle + 1, vertices_per_circle * 3);
}

sizes figures::circle(
        GLfloat radius,
        int vertices_per_circle,
        GLfloat *v_data,
        GLuint *i_data,
        int index_start,
        int stride) {

    auto segment = glm::pi<GLfloat>() / GLfloat(vertices_per_circle);
    for (int i = 0; i < vertices_per_circle; ++i) {
        GLfloat *vertex = &v_data[i * stride];
        // position
        vertex[0] = glm::sin(segment * GLfloat(i) * 2) * radius; // x
        vertex[1] = glm::cos(segment * GLfloat(i) * 2) * radius; // y
        vertex[2] = 0.0f; // z
        // normal
        vertex[3] = 0.0f; // x
        vertex[4] = 0.0f; // y
        vertex[5] = 1.0f; // z

        GLuint *triangle = &i_data[i * 3];
        triangle[0] = index_start + vertices_per_circle;
        triangle[1] = index_start + i;
        triangle[2] = index_start + (i + 1) % vertices_per_circle;
    }
    GLfloat *vertex = &v_data[vertices_per_circle * stride];
    // position
    vertex[0] = 0.0f;
    vertex[1] = 0.0f;
    vertex[2] = 0.0f;
    // normal
    vertex[3] = 0.0f; // x
    vertex[4] = 0.0f; // y
    vertex[5] = 1.0f; // z

    return circle_sizes(vertices_per_circle);
}


sizes figures::circle(
        GLfloat radius,
        int vertices_per_circle,
        GLfloat **v_data,
        GLuint **i_data,
        int index_start,
        int stride) {
    allocate(circle_sizes(vertices_per_circle), v_data, i_data, stride);
    return circle(radius, vertices_per_circle, *v_data, *i_data, index_start, stride);
}

sizes figures::cone_sizes(int vertices_per_circle) {
    return make_pair(vertices_per_circle + 1, vertices_per_circle * 3);
}

sizes figures::cone(
        GLfloat length,
        GLfloat radius,
        int vertices_per_circle,
        GLfloat *v_data,
        GLuint *i_data,
        int index_start,
        int stride) {

    auto segment = glm::pi<GLfloat>() / GLfloat(vertices_per_circle);
    for (int i = 0; i < vertices_per_circle; ++i) {
        GLfloat *vertex = &v_data[i * stride];
        // position
        vertex[0] = glm::sin(segment * GLfloat(i) * 2) * radius; // x
        vertex[1] = glm::cos(segment * GLfloat(i) * 2) * radius; // y
        vertex[2] = 0.0f; // z
        // normal
        GLfloat angle = glm::atan(radius, length);
        vertex[3] = glm::sin(segment * GLfloat(i) * 2) * glm::cos(angle); // x
        vertex[4] = glm::cos(segment * GLfloat(i) * 2) * glm::cos(angle); // y
        vertex[5] = glm::sin(angle); // z

        GLuint *triangle = &i_data[i * 3];
        triangle[0] = index_start + vertices_per_circle;
        triangle[1] = index_start + i;
        triangle[2] = index_start + (i + 1) % vertices_per_circle;
    }
    GLfloat *vertex = &v_data[vertices_per_circle * stride];
    // position
    vertex[0] = 0.0f;
    vertex[1] = 0.0f;
    vertex[2] = length;
    // normal
    vertex[3] = 0.0f; // x
    vertex[4] = 0.0f; // y
    vertex[5] = 1.0f; // z

    return cone_sizes(vertices_per_circle);
}


sizes figures::cone(
        GLfloat length,
        GLfloat radius,
        int vertices_per_circle,
        GLfloat **v_data,
        GLuint **i_data,
        int index_start,
        int stride) {
    allocate(cone_sizes(vertices_per_circle), v_data, i_data, stride);
    return cone(length, radius, vertices_per_circle, *v_data, *i_data, index_start, stride);
}

sizes figures::truncated_cone_sizes(int vertices_per_circle) {
    return make_pair(vertices_per_circle * 2 + 2, vertices_per_circle * 2 * 3);
}

sizes figures::truncated_cone(GLfloat length, GLfloat bottom_radius, GLfloat top_radius,
                              int vertices_per_circle, GLfloat *v_data, GLuint *i_data, int index_start, int stride) {

    auto segment = glm::pi<GLfloat>() / GLfloat(vertices_per_circle);
    auto angle = glm::atan(bottom_radius - top_radius, length);
    auto texture_segment = 1.0f / GLfloat(vertices_per_circle);
    for (int i = 0; i < vertices_per_circle; ++i) {
        GLfloat *vertex = &v_data[i * stride];
        // position
        vertex[0] = glm::sin(segment * GLfloat(i) * 2) * bottom_radius; // x
        vertex[1] = glm::cos(segment * GLfloat(i) * 2) * bottom_radius; // y
        vertex[2] = 0.0f; // z
        // normal
        vertex[3] = glm::sin(segment * GLfloat(i) * 2) * glm::cos(angle); // x
        vertex[4] = glm::cos(segment * GLfloat(i) * 2) * glm::cos(angle); // y
        vertex[5] = glm::sin(angle); // z
        // up
        vertex[6] = glm::sin(segment * GLfloat(i) * 2) * -glm::sin(angle); // x
        vertex[7] = glm::cos(segment * GLfloat(i) * 2) * -glm::sin(angle); // y
        vertex[8] = glm::cos(angle); // z
        // texture
        vertex[9] = texture_segment * GLfloat(i); // u
        vertex[10] = 0.0f; // v

        GLuint *triangle = &i_data[i * 3];
        triangle[0] = index_start + vertices_per_circle + i;
        triangle[1] = index_start + i;
        triangle[2] = index_start + (i + 1) % vertices_per_circle;
    }
    for (int i = vertices_per_circle; i < 2 * vertices_per_circle; ++i) {
        GLfloat *vertex = &v_data[i * stride];
        // position
        vertex[0] = glm::sin(segment * GLfloat(i) * 2) * top_radius; // x
        vertex[1] = glm::cos(segment * GLfloat(i) * 2) * top_radius; // y
        vertex[2] = length; // z
        // normal
        vertex[3] = glm::sin(segment * GLfloat(i) * 2) * glm::cos(angle); // x
        vertex[4] = glm::cos(segment * GLfloat(i) * 2) * glm::cos(angle); // y
        vertex[5] = glm::sin(angle); // z
        // up
        vertex[6] = glm::sin(segment * GLfloat(i) * 2) * -glm::sin(angle); // x
        vertex[7] = glm::cos(segment * GLfloat(i) * 2) * -glm::sin(angle); // y
        vertex[8] = glm::cos(angle); // z
        // texture
        vertex[9] = texture_segment * GLfloat(i % vertices_per_circle); // u
        vertex[10] = 2.0f; // v

        GLuint *triangle = &i_data[i * 3];
        triangle[0] = index_start + i;
        triangle[1] = index_start + (i + 1) % vertices_per_circle;
        triangle[2] = index_start + (i + 1) % vertices_per_circle + vertices_per_circle;
    }

    for (int i = 0; i < 2; ++i) {
        GLfloat *vertex = &v_data[(vertices_per_circle * 2 + i) * stride];
        // position
        vertex[0] = 0.0f; // x
        vertex[1] = i == 0 ? bottom_radius : top_radius; // y
        vertex[2] = i == 0 ? 0.0f : length; // z
        // normal
        vertex[3] = 0.0f * glm::cos(angle); // x
        vertex[4] = 1.0f * glm::cos(angle); // y
        vertex[5] = glm::sin(angle); // z
        // up
        vertex[6] = 0.0f * -glm::sin(angle); // x
        vertex[7] = 1.0f * -glm::sin(angle); // y
        vertex[8] = glm::cos(angle); // z
        // texture
        vertex[9] = 1.0f; // u
        vertex[10] = 2.0f * GLfloat(i); // v

        GLuint *triangle = &i_data[i == 0 ? (vertices_per_circle - 1) * 3 : (vertices_per_circle * 2 - 1) * 3];
        if (i == 0) {
            triangle[2] = index_start + vertices_per_circle * 2;
        } else {
            triangle[1] = index_start + vertices_per_circle * 2;
            triangle[2] = index_start + vertices_per_circle * 2 + 1;
        }
    }

    return truncated_cone_sizes(vertices_per_circle);
}

sizes figures::truncated_cone(GLfloat length, GLfloat bottom_radius, GLfloat top_radius, int vertices_per_circle,
                              GLfloat **v_data, GLuint **i_data, int index_start, int stride) {
    allocate(truncated_cone_sizes(vertices_per_circle), v_data, i_data, stride);
    return truncated_cone(length, bottom_radius, top_radius, vertices_per_circle, *v_data, *i_data, index_start, stride);
}

sizes figures::two_sided_rectangle_sizes() {
    return make_pair(8, 12);
}

sizes figures::two_sided_rectangle(GLfloat height, GLfloat width, GLfloat *v_data, GLuint *i_data, int index_start, int stride) {
    auto _sizes = two_sided_rectangle_sizes();
    for (int i = 0; i < 8; ++i) {
        GLfloat x = i % 2 == 0 ? 1.0f : -1.0f;
        GLfloat z = (i / 2) % 2 == 0 ? 0.0f : 1.0f;
        GLfloat normal_y = i < 4 ? 1.0f : -1.0f;

        GLfloat *vertex = &v_data[i * stride];
        // position
        vertex[0] = x * width / 2; // x
        vertex[1] = 0.0f; // y
        vertex[2] = z * height; // z
        // normal
        vertex[3] = 0.0f; // x
        vertex[4] = normal_y; // y
        vertex[5] = 0.0f; // z
        // up
        vertex[6] = 0.0f; // x
        vertex[7] = 0.0f; // y
        vertex[8] = 1.0f; // z
        // texture
        vertex[9] = 1.0f - z; // u
        vertex[10] = 0.5f + (normal_y / 4) + (x / 4); // v
    }

    i_data[0] = index_start + 0;
    i_data[1] = index_start + 2;
    i_data[2] = index_start + 1;
    i_data[3] = index_start + 1;
    i_data[4] = index_start + 2;
    i_data[5] = index_start + 3;

    i_data[6] = index_start + 4;
    i_data[7] = index_start + 5;
    i_data[8] = index_start + 6;
    i_data[9] = index_start + 5;
    i_data[10] = index_start + 7;
    i_data[11] = index_start + 6;
    return _sizes;
}

sizes figures::two_sided_rectangle(GLfloat height, GLfloat width,
                                   GLfloat **v_data, GLuint **i_data, int index_start, int stride) {
    allocate(two_sided_rectangle_sizes(), v_data, i_data, stride);
    return two_sided_rectangle(height, width, *v_data, *i_data, index_start, stride);
}
