#include "LSystem.h"

LSystem::LSystem(string _alphabet, std::initializer_list<std::pair<char, string>> l):
alphabet(_alphabet), rules(128) {
    char c ='+';
    for (int i = 0; i < 128; ++i) {
        rules[i] = string(1, char(i));
    }
    for (auto& elem : l) {
        rules[elem.first] = elem.second;
    }
}

string LSystem::produce(string axiom, int iterations) {
    auto current = axiom;
    auto next = string();
    for (int i = 0; i < iterations; ++i) {
        next = "";
        for (auto c : current) {
            next.append(rules[c]);
        }
        current = next;
    }
    return current;
}
