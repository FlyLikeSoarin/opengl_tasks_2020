#ifndef TREE_H
#define TREE_H

#include <vector>
#include <cstdlib>

#include "LSystem.h"
#include "Figures.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using std::vector;
using std::make_pair;


struct TreeGenerationState {
    TreeGenerationState();
    TreeGenerationState(glm::vec4 position, glm::vec4 front, glm::vec4 left, GLfloat length, GLfloat bottom_radius);

    glm::vec4 position;
    glm::vec4 front;
    glm::vec4 left;
    GLfloat length;
    GLfloat bottom_radius;
};


class BaseTree {
public:
    BaseTree(){}
    virtual ~BaseTree(){}

    void generate(LSystem l_system, string axiom, int iterations, GLfloat bottom_radius, GLfloat length);

    std::pair<GLfloat*, int> get_v_data_branch();
    std::pair<GLuint*, int> get_i_data_branch();

    std::pair<GLfloat*, int> get_v_data_leaf();
    std::pair<GLuint*, int> get_i_data_leaf();

protected:
    virtual void resolve_program_step(char step);
    void resolve_program_step_common(char step);
    void resolve_program_step_common(char step, GLfloat angle);
    void resolve_program_step_common(char step, GLfloat yaw, GLfloat pitch, GLfloat roll);
    void resolve_program_step_common(
            char step,
            GLfloat yaw_left, GLfloat yaw_right,
            GLfloat pitch_up, GLfloat pitch_down,
            GLfloat roll_ccw, GLfloat roll_cw
    );

    // Methods
    void pushState();
    void popState();
    TreeGenerationState& state();
    glm::mat4 current_branch_transform_matrix();
    glm::mat4 current_leaf_transform_matrix();

    void create_branch(GLfloat radius_mul=1.0f);
    void create_leaf(GLfloat length);

    // Fields
    vector<TreeGenerationState> state_stack;

    glm::vec3 up;
    glm::vec3 left;

    vector<GLfloat> v_data_branch;
    int v_data_branch_top = 0;
    vector<GLuint> i_data_branch;
    int i_data_branch_top = 0;

    vector<GLfloat> v_data_leaf;
    int v_data_leaf_top = 0;
    vector<GLuint> i_data_leaf;
    int i_data_leaf_top = 0;
};


#endif
