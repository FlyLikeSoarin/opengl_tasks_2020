#ifndef TREES_H
#define TREES_H


#include "BaseTree.h"


class GrassTree: public BaseTree {
protected:
    void resolve_program_step(char step) override;
};


class SympodialTree: public BaseTree {
protected:
    void resolve_program_step(char step) override;
};


#endif
