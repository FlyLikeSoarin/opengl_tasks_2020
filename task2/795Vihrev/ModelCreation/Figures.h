#ifndef FIGURES_H
#define FIGURES_H


#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/ext/scalar_constants.hpp>


namespace figures {

    typedef std::pair<int, int> sizes;

    void apply_transform(glm::mat4 transform_matrix, int vertices, GLfloat *v_data, int stride);

    sizes circle_sizes(int vertices_per_circle);

    sizes circle(
            GLfloat radius,
            int vertices_per_circle,
            GLfloat *v_data,
            GLuint *i_data,
            int index_start,
            int stride = 6
    );

    sizes circle(
            GLfloat radius,
            int vertices_per_circle,
            GLfloat **v_data,
            GLuint **i_data,
            int index_start,
            int stride = 6
    );

    sizes cone_sizes(int vertices_per_circle);

    sizes cone(
            GLfloat length,
            GLfloat radius,
            int vertices_per_circle,
            GLfloat *v_data,
            GLuint *i_data,
            int index_start,
            int stride = 6
    );

    sizes cone(
            GLfloat length,
            GLfloat radius,
            int vertices_per_circle,
            GLfloat **v_data,
            GLuint **i_data,
            int index_start,
            int stride = 6
    );

    sizes truncated_cone_sizes(int vertices_per_circle);

    sizes truncated_cone(GLfloat length, GLfloat bottom_radius, GLfloat top_radius,
                         int vertices_per_circle, GLfloat *v_data, GLuint *i_data, int index_start, int stride = 11);

    sizes truncated_cone(GLfloat length, GLfloat bottom_radius, GLfloat top_radius,
                         int vertices_per_circle, GLfloat **v_data, GLuint **i_data, int index_start, int stride = 11);

    sizes two_sided_rectangle_sizes();

    sizes two_sided_rectangle(GLfloat height, GLfloat width,
                               GLfloat *v_data, GLuint *i_data, int index_start, int stride = 11);

    sizes two_sided_rectangle(GLfloat height, GLfloat width,
                              GLfloat **v_data, GLuint **i_data, int index_start, int stride = 11);

}
#endif
