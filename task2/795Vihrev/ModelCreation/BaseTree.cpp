#include "BaseTree.h"


TreeGenerationState::TreeGenerationState() {}

TreeGenerationState::TreeGenerationState(glm::vec4 _position, glm::vec4 _front, glm::vec4 _left,
                                         GLfloat _length, GLfloat _bottom_radius)
        : position(_position), front(_front), left(_left), length(_length), bottom_radius(_bottom_radius) {}


void BaseTree::pushState() {
    state_stack.emplace_back(state().position,
                             state().front,
                             state().left,
                             state().length,
                             state().bottom_radius);
}

void BaseTree::popState() {
    state_stack.pop_back();
}

TreeGenerationState &BaseTree::state() {
    return state_stack.back();
}

glm::mat4 BaseTree::current_branch_transform_matrix() {
    auto dot = glm::dot(up, glm::vec3(state().front));
    auto cross = glm::normalize(glm::cross(up, glm::vec3(state().front)));

    if (glm::abs(glm::abs(dot) - 1.0f) < 0.00001f) {
        return glm::translate(glm::mat4(1.0f), glm::vec3(state().position))
               * glm::rotate(glm::mat4(1.0f), glm::acos(dot), left)
               * glm::rotate(glm::mat4(1.0f), glm::acos(glm::dot(left, glm::vec3(state().left))), up);
    } else {
        return glm::translate(glm::mat4(1.0f), glm::vec3(state().position))
               * glm::rotate(glm::mat4(1.0f), glm::acos(dot), cross)
               * glm::rotate(glm::mat4(1.0f), glm::acos(glm::dot(left, glm::vec3(state().left))), up);
    }
}

glm::mat4 BaseTree::current_leaf_transform_matrix() {
    return glm::translate(glm::mat4(1.0f), glm::vec3(state().position))
           * glm::rotate(glm::mat4(1.0f), GLfloat(rand())/GLfloat((RAND_MAX)) * glm::pi<GLfloat>(), up)
           * glm::rotate(glm::mat4(1.0f), glm::radians(90.0f) + (GLfloat(rand())/GLfloat((RAND_MAX)) - 0.5f) * glm::pi<GLfloat>() / 2, glm::vec3(1.0f, 0.0f, 0.0f))
           * glm::rotate(glm::mat4(1.0f), (GLfloat(rand())/GLfloat((RAND_MAX)) - 0.5f) * glm::pi<GLfloat>() / 4, up);
}

void BaseTree::create_branch(GLfloat radius_mul) {
    size_t free_v_space = v_data_branch.capacity() - v_data_branch_top;
    size_t free_i_space = i_data_branch.capacity() - i_data_branch_top;

    int vertices_per_circle = state_stack.size() < 4 ? 24 : 6;
    int stride = 11;
    auto sizes = figures::truncated_cone_sizes(vertices_per_circle);

    while (free_v_space < sizes.first * stride) {
        v_data_branch.reserve(v_data_branch.capacity() * 2);
        free_v_space = v_data_branch.capacity() - v_data_branch_top;
    }
    while (free_i_space < sizes.second) {
        i_data_branch.reserve(i_data_branch.capacity() * 2);
        free_i_space = i_data_branch.capacity() - i_data_branch_top;
    }

    v_data_branch.resize(v_data_branch.capacity());
    i_data_branch.resize(i_data_branch.capacity());

    figures::truncated_cone(state().length, state().bottom_radius, state().bottom_radius * radius_mul,
                            vertices_per_circle, &v_data_branch[v_data_branch_top], &i_data_branch[i_data_branch_top], v_data_branch_top / stride);

    figures::apply_transform(current_branch_transform_matrix(), sizes.first, &v_data_branch[v_data_branch_top], stride);
    v_data_branch_top += sizes.first * stride;
    i_data_branch_top += sizes.second;
}

void BaseTree::create_leaf(GLfloat length) {
    size_t free_v_space = v_data_leaf.capacity() - v_data_leaf_top;
    size_t free_i_space = i_data_leaf.capacity() - i_data_leaf_top;

    int stride = 11;
    auto sizes = figures::two_sided_rectangle_sizes();

    while (free_v_space < sizes.first * stride) {
        v_data_leaf.reserve(v_data_leaf.capacity() * 2);
        free_v_space = v_data_leaf.capacity() - v_data_leaf_top;
    }
    while (free_i_space < sizes.second) {
        i_data_leaf.reserve(i_data_leaf.capacity() * 2);
        free_i_space = i_data_leaf.capacity() - i_data_leaf_top;
    }

    v_data_leaf.resize(v_data_leaf.capacity());
    i_data_leaf.resize(i_data_leaf.capacity());

    figures::two_sided_rectangle(length, length / 2,
                                 &v_data_leaf[v_data_leaf_top], &i_data_leaf[i_data_leaf_top], v_data_leaf_top / stride);

    figures::apply_transform(current_leaf_transform_matrix(), sizes.first, &v_data_leaf[v_data_leaf_top], stride);
    v_data_leaf_top += sizes.first * stride;
    i_data_leaf_top += sizes.second;
}

void BaseTree::generate(LSystem l_system, string axiom, int iterations, GLfloat bottom_radius, GLfloat length) {
    GLfloat *vgfedg = nullptr;
    GLuint *kdfgdf = nullptr;
    figures::two_sided_rectangle(GLfloat(1.0f), GLfloat(1.0f), &vgfedg, &kdfgdf, 0);


    v_data_branch.reserve(128);
    i_data_branch.reserve(128);
    v_data_branch_top = 0;
    i_data_branch_top = 0;

    v_data_leaf.reserve(128);
    i_data_leaf.reserve(128);
    v_data_leaf_top = 0;
    i_data_leaf_top = 0;

    up = glm::vec3(0.0f, 0.0f, 1.0f);
    left = glm::vec3(0.0f, 1.0f, 0.0f);

    state_stack.clear();
    state_stack.emplace_back(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), glm::vec4(up, 0.0f), glm::vec4(left, 0.0f), length,
                             bottom_radius);


    auto program = l_system.produce(axiom, iterations);
    for (char step : program) {
        resolve_program_step(step);
    }
}

void BaseTree::resolve_program_step(char step) {
    return;
}

void BaseTree::resolve_program_step_common(char step) {
    resolve_program_step_common(step, 45.0f, -45.0f, 45.0f, -45.0f, 45.0f, -45.0f);
}

void BaseTree::resolve_program_step_common(char step, GLfloat angle) {
    resolve_program_step_common(step, angle, -angle, angle, -angle, angle, -angle);
}

void BaseTree::resolve_program_step_common(char step, GLfloat yaw, GLfloat pitch, GLfloat roll) {
    resolve_program_step_common(step, yaw, -yaw, pitch, -pitch, roll, -roll);
}

void BaseTree::resolve_program_step_common(
        char step,
        GLfloat yaw_left, GLfloat yaw_right,
        GLfloat pitch_up, GLfloat pitch_down,
        GLfloat roll_ccw, GLfloat roll_cw
) {
    switch (step) {
        case '[': {
            pushState();
            return;
        }
        case ']': {
            popState();
            return;
        }
        case '+': {
            auto local_up = glm::normalize(glm::cross(glm::vec3(state().front), glm::vec3(state().left)));
            auto rotate = glm::rotate(glm::mat4(1.0f), glm::radians(yaw_left), local_up);
            state().front = glm::normalize(rotate * state().front);
            state().left = glm::normalize(rotate * state().left);
            return;
        }
        case '-': {
            auto local_up = glm::normalize(glm::cross(glm::vec3(state().front), glm::vec3(state().left)));
            auto rotate = glm::rotate(glm::mat4(1.0f), glm::radians(yaw_right), local_up);
            state().front = glm::normalize(rotate * state().front);
            state().left = glm::normalize(rotate * state().left);
            return;
        }
        case '&': {
            state().front = glm::normalize(
                    glm::rotate(
                            glm::mat4(1.0f), glm::radians(pitch_up), glm::vec3(state().left)
                    ) * state().front
            );
            return;
        }
        case '^': {
            state().front = glm::normalize(
                    glm::rotate(
                            glm::mat4(1.0f), glm::radians(pitch_down), glm::vec3(state().left)
                    ) * state().front
            );
            return;
        }
        case '/': {
            state().left = glm::normalize(
                    glm::rotate(
                            glm::mat4(1.0f), glm::radians(roll_ccw), glm::vec3(state().front)
                    ) * state().left
            );
            return;
        }
        case '\\': {
            state().left = glm::normalize(
                    glm::rotate(
                            glm::mat4(1.0f), glm::radians(roll_cw), glm::vec3(state().front)
                    ) * state().left
            );
            return;
        }
        case '$': {
            auto local_front = glm::normalize(glm::cross(up, glm::vec3(state().left)));
            auto angle = glm::asin(glm::dot(up, glm::vec3(state().left)));
            state().left = glm::normalize(
                    glm::rotate(glm::mat4(1.0f), angle, local_front) * state().left);
            return;
        }
        default:
            return;
    }
}

std::pair<GLfloat *, int> BaseTree::get_v_data_branch() {
    return make_pair(&v_data_branch[0], v_data_branch_top);
}

std::pair<GLuint *, int> BaseTree::get_i_data_branch() {
    return make_pair(&i_data_branch[0], i_data_branch_top);
}

std::pair<GLfloat *, int> BaseTree::get_v_data_leaf() {
    return make_pair(&v_data_leaf[0], v_data_leaf_top);
}

std::pair<GLuint *, int> BaseTree::get_i_data_leaf() {
    return make_pair(&i_data_leaf[0], i_data_leaf_top);
}
