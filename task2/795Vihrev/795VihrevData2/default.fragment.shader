#version 330

in vec2 TextureCoords;
in vec3 Light;
out vec4 color;

uniform sampler2D u_TextureSampler;
uniform sampler2D u_NormalSampler;

uniform int u_UseAlpha;
uniform sampler2D u_AlphaSampler;

void main()
{
    if (!gl_FrontFacing) {
        discard;
    }
    vec3 textureNormal = vec3(texture(u_NormalSampler, TextureCoords)) * 2.0 - 1.0;
    float brightness = (dot(textureNormal, Light) + 1.0) / 2.0;
    if (u_UseAlpha == 0) {
        color = vec4(vec3(texture(u_TextureSampler, TextureCoords) * brightness), 1.0);
    } else {
        float alpha = texture(u_AlphaSampler, TextureCoords).x;
        if (alpha < 0.1) {
            discard;
        }
        color = vec4(vec3(texture(u_TextureSampler, TextureCoords) * brightness), alpha);
    }
}
